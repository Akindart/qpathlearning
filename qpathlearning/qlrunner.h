#ifndef QLRUNNER_H
#define QLRUNNER_H

#include <QObject>
#include <QString>
#include "QLA/problem.h"
#include "QLA/filereader.h"
#include "QLA/qlearningsolver.h"
#include "world.h"
#include <string>

class QLRunner : public QObject
{
    Q_OBJECT


public:
    explicit QLRunner(QObject *parent = 0);

    Q_INVOKABLE QString getProblemSolved(QString qlfPath, QString iterations, QString alpha, QString gamma, QString rho, QString nu, QString goal, World *world, QString additionalReward, QString initialState);



signals:

public slots:

};

#endif // QLRUNNER_H
