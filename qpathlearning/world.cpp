#include "world.h"
#include <iostream>

World::World(QObject *parent) :
    QObject(parent)
{

    this->currentCellObtainableColor = 0;
    int max = 40*28;
    this->cell_list.clear();

    this->cost_grass_value = 0.05;
    this->cost_forest_value = 0.1;
    this->cost_water_value = 0.14;
    this->cost_swamp_value = 0.17;
    this->cost_earth_value = 0.06;
    this->cost_another_value = 0.07;

    this->distance_weight_value = 0.5;



}

double World::getStatesManhatanDistance(int state1, int state2){

    int x_s1 = state1/28, x_s2 = state2/28;
    int y_s1 = state1%28, y_s2 = state2%28;

    return (x_s2-x_s1)+ (y_s2-y_s1);

}

double World::getStatesDistance(int state1, int state2){

    int x_s1 = state1/28, x_s2 = state2/28;
    int y_s1 = state1%28, y_s2 = state2%28;

    return sqrt((x_s2-x_s1)*(x_s2-x_s1) + (y_s2-y_s1)*(y_s2-y_s1));

}

double World::calculateCost(double effort, double distance){
    
    //if(distance = 0) return 100000;
    
    return ((1/(this->distance_weight_value*distance))*(1-effort)*(1-effort))*1000;

}



void World::setCost_grass(QString value)
{
    float temp = value.toFloat();
    this->cost_grass_value = temp;
}

void World::setCost_forest(QString value)
{
    float temp = value.toFloat();
    this->cost_forest_value = temp;
}

void World::setCost_water(QString value)
{
    float temp = value.toFloat();
    this->cost_water_value = temp;
}

void World::setCost_swamp(QString value)
{
    float temp = value.toFloat();
    this->cost_swamp_value = temp;
}

void World::setCost_earth(QString value)
{
    float temp = value.toFloat();
    this->cost_earth_value = temp;
}

void World::setCost_another(QString value)
{
    float temp = value.toFloat();
    this->cost_another_value = temp;
}

void World::setDistance_weight(QString value)
{
    float temp = value.toFloat();
    this->distance_weight_value = temp;
}

QColor World::getRectCellColor()
{
    this->currentCellObtainableColor++;
    return this->cell_list.value(this->worldPoints.at(this->currentCellObtainableColor-1))->color_original;
}

//void World::addCell(QColor color)
//{
//    const rectCell tempCell(color, this);

//    this->cellList.append(tempCell);
//}

double World::getCost(const QColor &color, int state, int goal)
{

    double distance = this->getStatesDistance(goal, state);

    if(distance == 0) {

        distance = 0.00001;
        std::cout<< "SHOUT!!!!!!! State : " << state << std::endl;

    }
    if(color.name() == "#14cd22")//grass
        //return calculateCost(0.05, this->getStatesManhatanDistance(goal, state));
        return calculateCost(this->cost_grass_value, distance);

    else if(color.name() == "#063c0b")//forest
        //return calculateCost(0.4, this->getStatesManhatanDistance(goal, state));
        return calculateCost(this->cost_forest_value, distance);

    else if(color.name() == "#0a028e")//water
        //return calculateCost(0.5, this->getStatesManhatanDistance(goal, state));
        return calculateCost(this->cost_water_value, distance);

    else if(color.name() == "#4a2500")//swamp
        //return calculateCost(0.8, this->getStatesManhatanDistance(goal, state));
        return calculateCost(this->cost_swamp_value, distance);

    else if(color.name() == "#ff8000")//earth
        //return calculateCost(0.06, this->getStatesManhatanDistance(goal, state));
        return calculateCost(this->cost_earth_value, distance);

    else //return calculateCost(0.2, this->getStatesManhatanDistance(goal, state));
        return calculateCost(this->cost_another_value, distance);

    //anyting

}

QString World::getCurrentCellColorString()
{
    this->currentCellObtainableColor++;
    return this->worldColorsS.at(this->currentCellObtainableColor-1);
}

QColor World::getCurrentCellColor()
{
    this->currentCellObtainableColor++;
    return this->worldPointColors.value(this->worldPoints.at(this->currentCellObtainableColor-1));
}

QString World::getHSVColor(QColor color)
{

    return color.toHsv().name();
}

QList<QColor> World::getWorldColors()
{
    return this->worldColors;
}

QStringList World::getWorldColorsS()
{
    return this->worldColorsS;
}

void World::writeToFileWorldStates(QString filePath, int goal, int additionalReward)
{
    QFile qfile(filePath);

    qfile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&qfile);

    out << this->worldPoints.size() << "\n";

    int addRwrd = additionalReward;

    for(int col=0; col<40; col++)
        for(int row=0; row<28; row++){

            if(row-1 >= 0 && goal == col*28+(row-1)){

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at(col*28 + (row-1))), col*28+(row-1), goal);
                out << col*28+row << " " << col*28+(row-1) << " " << cost + addRwrd << QString("\n");

            }
            else if(row-1 >= 0){

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at(col*28 + (row-1))), col*28+(row-1), goal);
                out << col*28+row << " " << col*28+(row-1) << " " << cost << QString("\n");

            }

            if(row+1 < 28 && goal == col*28+(row+1)){

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at(col*28 + (row+1))), col*28+(row+1), goal);
                out << col*28+row << " " << col*28+(row+1) << " " << cost + addRwrd << QString("\n");

            }

            else if(row+1 < 28){

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at(col*28 + (row+1))), col*28+(row+1), goal);
                out << col*28+row << " " << col*28+(row+1) << " " << cost << QString("\n");

            }

            if(col-1 >= 0 && goal == (col-1)*28+row){

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at((col-1)*28 + row)), (col-1)*28+row, goal);
                out << col*28+row << " " << (col-1)*28+row << " " << cost + addRwrd  << QString("\n");

            }
            else if(col-1 >= 0){

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at((col-1)*28 + row)), (col-1)*28+row, goal);
                out << col*28+row << " " << (col-1)*28+row << " " << cost << QString("\n");

            }

            if(col+1 < 40 && goal == (col+1)*28+row){

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at((col+1)*28 + row)), (col+1)*28+row, goal);
                out << col*28+row << " " << (col+1)*28+row << " " << cost + addRwrd  << QString("\n");

            }
            else if(col+1 < 40) {

                int cost = this->getCost(this->worldPointColors.value(this->worldPoints.at((col+1)*28 + row)), (col+1)*28+row, goal);
                out << col*28+row << " " << (col+1)*28+row << " " << cost  << QString("\n");

            }

        }

    qfile.close();

}

void World::addToWorldColorsString(QString colorName)
{
    this->worldColorsS.append(colorName);

}

void World::addToWorldColors(QColor color, int x, int y)
{

    //    if(x > 0 && y > 0)
    //        std::cout<< this->worldPointColors.begin().value().hue() <<.
    //                    " < " << color.hue() << " : "
    //                 << (this->worldPointColors.begin().value().value() < color.value())
    //                 << std::endl;

    //    std::cout << "Pixel (" << x/16 << " ," << y/16 << ") ==>"
    //              <<"COLOR IN HSV -- color->hue() = " << color.hue() << "; color->value() = "
    //              << color.value() << "; color->saturation() = " << color.saturation() << std::endl;


    if(x != 0 && divX == 1){

        divX = x;
        std::cout << "dix " <<  divX << std::endl;

    }if(y != 0 && divY == 1) divY = y;

    //if(this->getCost(&color) != -2) std::cout << "Pixel (" << x/divX << " ," << y/divY << ") ==>" << color.name().toStdString() << std::endl;
    //if(this->getCost(&color) == -2) std::cout << "Pixel with different color (" << x/16 << " ," << y/16 << ") ==>" << color.name().toStdString() << std::endl;
    this->worldPoints.append(new QPoint(x/divX, y/divY));

    this->cell_list.insert(this->worldPoints.last(), new rectCell(color, this));
    this->worldPointColors.insert(this->worldPoints.last(), color);

}

int World::goal()
{

    return goal_value;

}

void World::setGoal(QString value)
{

    int temp = value.toInt();
    this->goal_value = temp;

}

void World::setProblem(Problem *problem)
{

    this->problem = problem;

}

Problem *World::getProblem()
{

    return this->problem;

}

QList<QPoint *> *World::getPoints()
{
    return &this->worldPoints;
}
