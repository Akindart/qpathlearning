#ifndef WORLD_H
#define WORLD_H

#include <QObject>
#include <QStringList>
#include <QColor>
#include <QFile>
#include <QTextStream>
#include <QHash>
#include <QPoint>
#include <math.h>
#include "QLA/problem.h"
#include "rectcell.h"


class World : public QObject
{
    Q_OBJECT

    QStringList worldColorsS;
    QHash<QPoint *, QColor> worldPointColors;
    QList<QPoint *> worldPoints;
    QList<QColor> worldColors;


    int currentCellObtainableColor;

    double getCost(const QColor &color, int state, int goal);

    Q_PROPERTY(int goal READ goal)

    Problem *problem;

    int height;
    int width;

    float cost_grass_value;
    float cost_forest_value;
    float cost_water_value;
    float cost_swamp_value;
    float cost_earth_value;
    float cost_another_value;

    float distance_weight_value;

    int goal_value;

public:
    explicit World(QObject *parent = 0);

    QHash<QPoint *, rectCell *> cell_list;

    Q_INVOKABLE QString getCurrentCellColorString();
    Q_INVOKABLE QColor getCurrentCellColor();
    Q_INVOKABLE QString getHSVColor(QColor color);

    QList<QColor> getWorldColors();
    QStringList getWorldColorsS();

    Q_INVOKABLE void writeToFileWorldStates(QString filePath, int goal, int additionalReward);
    //Q_INVOKABLE void createWorldWithPoints(QString imagePath, World *world);

    void addToWorldColorsString(QString colorName);

    void addToWorldColors(QColor color, int x, int y);

    int getHeight();//to do
    int getWidth();//to do

    void setHeight(int height);//to do
    void setWidth(int width);//to do

    int goal();
    Q_INVOKABLE void setGoal(QString value);

    void setProblem(Problem *problem);
    Problem *getProblem();

    QList<QPoint *> *getPoints();

    int divX = 1;
    int divY = 1;

    double getStatesManhatanDistance(int state1, int state2);

    double getStatesDistance(int state1, int state2);

    double calculateCost(double effort, double distance);

    QString cost_grass();
    QString cost_forest();
    QString cost_water();
    QString cost_swamp();
    QString cost_earth();
    QString cost_another();

    QString distance_weight();

    Q_INVOKABLE void setCost_grass(QString value);
    Q_INVOKABLE void setCost_forest(QString value);
    Q_INVOKABLE void setCost_water(QString value);
    Q_INVOKABLE void setCost_swamp(QString value);
    Q_INVOKABLE void setCost_earth(QString value);
    Q_INVOKABLE void setCost_another(QString value);

    Q_INVOKABLE void setDistance_weight(QString value);

    Q_INVOKABLE QColor getRectCellColor();


    void addCell(QColor color);

signals:


public slots:



};

#endif // WORLD_H
