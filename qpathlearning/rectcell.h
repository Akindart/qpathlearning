#ifndef RECTCELL_H
#define RECTCELL_H

#include <QObject>
#include <QColor>

class rectCell : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QColor colorValue READ colorValue WRITE setColorValue NOTIFY colorValueChanged)
    Q_PROPERTY(QColor tempColor READ tempColor)

public:
    explicit rectCell(QColor color, QObject *parent = 0);


    QColor color_original;
    QColor temp_color;

    Q_INVOKABLE QColor colorValue();
    Q_INVOKABLE QColor tempColor();
    void setColorValue(QColor color);

signals:

    void colorValueChanged();

public slots:

};

#endif // RECTCELL_H
