#ifndef PROBLEM_H
#define PROBLEM_H

#include "graphproblem.h"

/*
 *
 *This object represents the problem itself, with its own policy and its own representation,
 *in this case, a graph. *
 *
 */

class Problem
{
public:
    Problem();

    Problem(int numStates);

    GraphProblem *getThisGraphProblem();

    Policy *getMyPolicy();

    Policy *getQStore();

    State *getRandomState();

private:

    GraphProblem * thisGraphProblem;

    Policy *myPolicy;

    Policy *QStore;

};

#endif // PROBLEM_H
