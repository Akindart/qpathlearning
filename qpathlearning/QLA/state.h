#ifndef STATE_H
#define STATE_H

#include <iostream>
#include <map>
#include <vector>
#include <string>
#include "state.h"
#include "Utils.h"

//to see what's the reward to walk to the state especifed

typedef int numberOfState;

class State
{
public:

    State(numberOfState numState);

    numberOfState getNumState();//recover the number of the state

    bool operator<(State other);
    bool operator==(State other);


private:

    numberOfState numState;    

};

#endif // STATE_H


