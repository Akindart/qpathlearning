#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>

namespace Utils {

extern int random(int min, int max);

extern double normalizer(double min, double max);

}



#endif // UTILS_H
