#include "qlearningsolver.h"
#include <iostream>

QLearningSolver::QLearningSolver()
{



}

int QLearningSolver::QLearningAlgorithm(Problem *problem, int iterations, float alpha, float gamma, float rho, float nu, int goal, int initialState)
{
    State *state;

    if(initialState == -1)
       state = problem->getRandomState();
    else state = problem->getThisGraphProblem()->getStateX(initialState);

    std::cout << "INITIAL STATE : " <<"state: " << state->getNumState() << std::endl;

    for(int i=0; i < iterations; i++){

        //pick a new state once in a while
        if(Utils::normalizer(Utils::random(0, problem->getThisGraphProblem()->getProblemStates()->size()), problem->getThisGraphProblem()->getProblemStates()->size()) <= nu)
            state = problem->getRandomState();


        //get available actions for this state
        Actions *actions = problem->getMyPolicy()->getPolicyValuesForStateX(state->getNumState());

        nextStateRewardPair action;

        //check to see if it'll be used a random action this rime
        if((int)Utils::normalizer(Utils::random(0, problem->getThisGraphProblem()->getProblemStates()->size()), problem->getThisGraphProblem()->getProblemStates()->size()) < rho)
            action = actions->randomAction();
        else action = actions->findMaxReward(); //pick the best action



        double reward = action->second;
        State *nextState = action->first;

        //std::cout << state->getNumState() << " --> " << nextState->getNumState() <<  " : " << reward << std::endl;


        double Q = problem->getQStore()->getPolicyValuesForStateX(state->getNumState())->getRewardByGoingToStateX(nextState);

        double maxQ = problem->getQStore()->getPolicyValuesForStateX(nextState->getNumState())->findMaxReward()->second;

        Q = (1-alpha)*Q + alpha*(reward + gamma*maxQ);

        //std::cout << "QValue after calculation : " << state->getNumState() << " --> " << nextState->getNumState() <<  " : " << Q << std::endl;


        Actions *tempActionPointer = problem->getQStore()->getPolicyMap()->at(state->getNumState());

        tempActionPointer->insertValue(nextState, Q);

        state = nextState;

        if(state->getNumState() == goal ){

            //std::cout<<"Cheguei no meu destino"<<std::endl;
            //system("pause");
            if(initialState == -1)
               state = problem->getRandomState();
            else state = problem->getThisGraphProblem()->getStateX(initialState);


        }
    }

    return 0;

}

