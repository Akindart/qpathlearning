#ifndef FILEREADER_H
#define FILEREADER_H

#include "problem.h"
#include <string>
#include <iomanip>
#include <fstream>
#include <cstdlib>
using namespace std;

class FileReader
{
public:
    FileReader();

    Problem *ReadFile(const string fileName);

    void outputLine(int numState, int LinkState, double Reward);

};

#endif // FILEREADER_H
