#ifndef QSOLVER_H
#define QSOLVER_H

#include "problem.h"

class QLearningSolver
{
public:

    QLearningSolver();

    int QLearningAlgorithm(Problem *Problem, int iterations, float alpha, float gamma, float rho, float nu, int goal, int initialState);

};

#endif // QSOLVER_H
