#ifndef GRAPHPROBLEM_H
#define GRAPHPROBLEM_H


#include "policy.h"


typedef std::vector<State *> *states;

class GraphProblem
{
public:
    GraphProblem();

    GraphProblem(int numStates);

    State *getRandomState();

    State *getStateX(numberOfState numStateX);

    states getProblemStates();


private:

    states problemStates;



};

#endif // GRAPHPROBLEM_H
