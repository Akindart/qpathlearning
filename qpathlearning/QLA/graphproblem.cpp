#include "graphproblem.h"

GraphProblem::GraphProblem()
{

    this->problemStates = new std::vector<State *>();

}

GraphProblem::GraphProblem(int numStates)
{

    this->problemStates = new std::vector<State *>();

    for(int i=0; i<numStates; i++){

        this->problemStates->push_back(new State(i));

    }

}

State *GraphProblem::getRandomState()
{

    if(!this->problemStates->empty()){

        int randomState = Utils::random(0, this->problemStates->size()-1);
        //std::cout << "RandomState: " << randomState << std::endl;
        return this->problemStates->at(randomState);

    }
    else return NULL;

}

State *GraphProblem::getStateX(numberOfState numStateX)
{

    return this->problemStates->at(numStateX);

}

states GraphProblem::getProblemStates()
{

    return this->problemStates;

}
