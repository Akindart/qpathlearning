#ifndef QVALUES_H
#define QVALUES_H

#include <iostream>
#include <map>
#include <iomanip>
#include <string>
#include <sstream>
#include "state.h"

typedef std::map<State *, double > *nextRewardsByGoingToStateX;
typedef std::map<State *, double >::value_type *nextStateRewardPair;

class Actions
{

public:
    Actions();

    double getRewardByGoingToStateX(State * StateX);//recover the reward received by going to the X state

    nextRewardsByGoingToStateX getRewardMap();//picks the row of the of rewards for a certain state


    nextStateRewardPair findMaxRewardWithoutZeros();

    nextStateRewardPair findMaxReward();//find max reward of having some action "a" from state X


    nextStateRewardPair randomAction();//just pick a random action

    bool insertValue(State *action, double reward);

    std::string toString(int maxStates);

    std::string toString();

private:

    nextRewardsByGoingToStateX rewardMap; //the row of rewards

};

#endif // QVALUES_H
