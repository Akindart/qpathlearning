#include "filereader.h"

FileReader::FileReader()
{
}

Problem *FileReader::ReadFile(const string fileName)
{

    ifstream inProblemFile(fileName, ios::in);

    if(!inProblemFile){

        cerr << "File could not be opened" << endl;
        exit(1);

    }

    int qtyStates;
    int numState;
    int LinkState;
    double Reward;

    inProblemFile >> qtyStates;

    Problem *problem = new Problem(qtyStates);

    cout << left << "Number of States: "<< qtyStates << endl << fixed << showpoint;

//    cout << left << setw(10) << "State origin"<< setw(10) <<
//            "   -->  "<< setw(10) <<"TargetState  "<< setw(10) <<
//            " Reward" << endl << fixed << showpoint;

    while(inProblemFile >> numState >> LinkState >> Reward){

        //this->outputLine(numState, LinkState, Reward);
        State *tempStateTarget = problem->getThisGraphProblem()->getProblemStates()->at(LinkState);

        //cout<<"passei do tempStatetarget: "<< tempStateTarget->getNumState() <<endl;

        //creating policy
        problem->getMyPolicy()->getPolicyMap()->insert(std::pair<int, Actions *>(numState, new Actions()));

        //cout<<"passei da criacao do par std::pair<int, Actions *>(" << numState << ", new Acitons())" << endl;

        Actions *tempActions;

        tempActions = problem->getMyPolicy()->getPolicyMap()->at(numState);

        //cout<<"passei da associacao tempActions"<<endl;

        tempActions->getRewardMap()->insert(std::pair<State *, double>(tempStateTarget, Reward));

        //cout<<"criei o par std::pair<State *, int>: ("<< tempStateTarget->getNumState() << ", " << Reward << ")" << endl;

        //creating Qstore
        problem->getQStore()->getPolicyMap()->insert(std::pair<int, Actions *>(numState, new Actions()));
        tempActions = problem->getQStore()->getPolicyMap()->at(numState);
        tempActions->getRewardMap()->insert(std::pair<State *, double>(tempStateTarget, 0));


    }


    //cout << "problem->getThisGraphProblem()->getProblemStates()->size() : " << problem->getThisGraphProblem()->getProblemStates()->size() << endl;

    return problem;

}

void FileReader::outputLine(int numState, int LinkState, double Reward)
{

    cout << left << setw(15) << numState << setw(17) <<
            "-->"<< setw(3) << LinkState << setw(7) <<
           right << Reward <<endl;

}

