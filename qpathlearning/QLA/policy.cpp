#include "policy.h"
#include <iomanip>
#include <fstream>


using namespace std;

Policy::Policy(policyMap PolicyMap)
{

    this->myPolicyMap = PolicyMap;

}

Policy::Policy()
{

    this->myPolicyMap = new std::map< numberOfState, Actions *>();

}

Actions *Policy::getPolicyValuesForStateX(numberOfState NumState)
{

    return this->getPolicyMap()->at(NumState);

}

policyMap Policy::getPolicyMap()
{

    return this->myPolicyMap;

}

std::string Policy::toString(int iterations, double alpha, double gamma)
{

    std::stringstream sstring;

    sstring << endl << "QValues after "<< iterations << " iterations, with alpha value "<< alpha << " and gamma value " << gamma << endl << endl;

    sstring << setw(3) << "" <<std::left << setw(12) << "";

    for(int i=0; i < this->getPolicyMap()->size(); i++){

        sstring << setw(11) << i+1;

    }

    sstring << "\n\n" <<std::endl;

    for(std::map<numberOfState, Actions *>::value_type tempPair : *(this->getPolicyMap())){

        sstring << left << setw(4) <<tempPair.first + 1 ;
        sstring << tempPair.second->toString(this->getPolicyMap()->size());
        sstring << "\n\n\n";

    }

    sstring << std::endl;

    return sstring.str();

}

string Policy::toString_2(int iterations, double alpha, double gamma)
{

    std::stringstream sstring;

    sstring << endl << "QValues after "<< iterations << " iterations, with alpha value "<< alpha << " and gamma value " << gamma << endl << endl;

    for(std::map<numberOfState, Actions *>::value_type tempPair : *(this->getPolicyMap())){

        sstring << left << setw(3) <<tempPair.first + 1 ;
        sstring << tempPair.second->toString();
        sstring << "\n\n\n";

    }

    sstring << std::endl;

    return sstring.str();

}
