#include "Utils.h"

namespace Utils {

int random(int min, int max)
{
    int range, result, cutoff;

    if (min >= max)
        return min; // only one outcome possible, or invalid parameters
    range = max-min+1;
    cutoff = (RAND_MAX / range) * range;

    // Rejection method, to be statistically unbiased.
    do {
        result = rand();
    } while (result >= cutoff);

    return result % range + min;
}

double normalizer(double min, double max)
{

    return (max-min)/max;

}



}

