#include "actions.h"
#include <math.h>

using namespace std;

Actions::Actions()
{

    this->rewardMap = new std::map<State *, double>();

}

double Actions::getRewardByGoingToStateX(State *StateX)
{
    return this->getRewardMap()->at(StateX);
}

nextRewardsByGoingToStateX Actions::getRewardMap()
{
    return this->rewardMap;
}

nextStateRewardPair Actions::findMaxReward()
{
    std::vector<State *> * tempList = new std::vector<State *>();
    std::map<State *, double>::value_type * maxReward = &*this->getRewardMap()->begin();

    //as long as no value can assume some value less than zero


    for(std::map<State *, double>::value_type tempPair : *(this->getRewardMap()) ){//search each action and each reward value

        if(maxReward->second < tempPair.second && maxReward->second != tempPair.second){//if the current maxReward is lesser than the curret Pair that is being looked at

            maxReward = &tempPair;
            free(tempList);
            tempList = new std::vector<State *>();
            tempList->push_back(maxReward->first);


        }
        else if(maxReward->second == tempPair.second){//if the max reward repeats, we put all states at a list and choose pseudo randomly one to go

            tempList->push_back(maxReward->first);

        }

    }

    if(tempList->size() > 1){//select next state by pseudo random process

        int pos_x, pos_x_nearest;
        int pos_y, pos_y_nearest;

        int pos_list = 0;

        pos_x_nearest = tempList->at(0)->getNumState()/28;
        pos_y_nearest = tempList->at(0)->getNumState()%28;

        //std::cout << "aqui" << std::endl;

//        for(int i = 0; i < tempList->size(); i++){

//            pos_x = tempList->at(i)->getNumState()/28;
//            pos_y = tempList->at(i)->getNumState()%28 - 16;

//            if(sqrt(pos_x*pos_x + pos_y*pos_y) < sqrt(pos_x_nearest*pos_x_nearest + pos_y_nearest*pos_y_nearest)){

//                pos_x_nearest = pos_x;
//                pos_y_nearest = pos_y;
//                pos_list = i;

//            }

//        }

        if(tempList->size() > 1){//select next state by pseudo random process

                int choosenState = Utils::random(0, tempList->size()-1);

                return &*(this->getRewardMap()->find(tempList->at(choosenState)));

         }


        return &*(this->getRewardMap()->find(tempList->at(pos_list)));

    }

    else return maxReward; //if everything fails

}



//nextStateRewardPair Actions::findMaxReward()
//{

//    std::vector<State *> * tempList = new std::vector<State *>();
//    std::map<State *, double>::value_type * maxReward = &*this->getRewardMap()->begin();

//    for(std::map<State *, double>::value_type tempPair : *(this->getRewardMap()) )
//        if(tempPair.second != 0){
//            maxReward = &tempPair;
//            break;
//        }

//    //as long as no value can assume some value less than zero

//    for(std::map<State *, double>::value_type tempPair : *(this->getRewardMap()) ){//search each action and each reward value

//        if(maxReward->second < tempPair.second && maxReward->second != tempPair.second && tempPair.second != 0){//if the current maxReward is lesser than the curret Pair that is being looked at

//            maxReward = &tempPair;
//            free(tempList);
//            tempList = new std::vector<State *>();
//            tempList->push_back(maxReward->first);


//        }
//        else if(maxReward->second == tempPair.second){//if the max reward repeats, we put all states at a list and choose pseudo randomly one to go

//            tempList->push_back(maxReward->first);

//        }

//    }

//    if(tempList->size() > 1){//select next state by pseudo random process

//        int choosenState = Utils::random(0, tempList->size()-1);

//        return &*(this->getRewardMap()->find(tempList->at(choosenState)));

//    }

//    else return maxReward; //if everything fails

//}


nextStateRewardPair Actions::randomAction()
{

    std::vector<nextStateRewardPair> tempList;

    for(std::map<State *, double>::value_type tempPair : *(this->getRewardMap()) )
        tempList.push_back(&tempPair);

    return tempList.at(Utils::random(0, tempList.size()-1));


}

bool Actions::insertValue(State *action, double reward)
{

    //std::map<State *, int >::iterator it = this->rewardMap->begin();
    std::pair<std::map<State *, double>::iterator,bool> ret;

    ret = this->rewardMap->insert(std::pair<State *, double >(action, reward));
    if(ret.second == false){

        this->rewardMap->erase(action);
        this->rewardMap->insert(std::pair<State *, double >(action, reward));

    }

    return true;

}

string Actions::toString(int maxStates)
{

    int auxInt = 0 ;

    stringstream sstring;

    sstring << setw(1) << "" << left << fixed <<setprecision(3);

    for(std::map<State *, double>::value_type tempPair : *(this->getRewardMap()) ){

        State *state = tempPair.first;

        if(state->getNumState() > auxInt){

            int i=0;

            while(i<state->getNumState() - auxInt){

                sstring << setw(6) << "" << 0.000 << left << fixed << setprecision(3) ;

                i++;

            }

        }

        auxInt = state->getNumState() +1 ;

        sstring<< setw(6) << "" << tempPair.second <<left << fixed << setprecision(3);

    }

    for(int i=0; i<maxStates - auxInt; i++)
        sstring<< setw(6) << "" << 0.000 << left;

    return sstring.str();

}

std::string Actions::toString()
{

    int auxInt = 0 ;

    stringstream sstring;

    sstring << setw(1) << "" << left << fixed <<setprecision(3);

    for(std::map<State *, double>::value_type tempPair : *(this->getRewardMap()) ){

        if(tempPair.second > 0)  sstring << setw(6) << "" << tempPair.first->getNumState()+1 << left << fixed << setprecision(3) ;

    }

    sstring << "\n";

    for(std::map<State *, double>::value_type tempPair : *(this->getRewardMap()) ){

        if(tempPair.second > 0)  sstring << setw(6) << "" << tempPair.second << left << fixed << setprecision(3) ;

    }

    return sstring.str();


}


