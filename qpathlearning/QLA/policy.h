#ifndef POLIC_H
#define POLIC_H


#include <iostream>
#include <map>
#include <vector>
#include "actions.h"


typedef std::map< numberOfState, Actions *> * policyMap;//int is the number of the state, therefore it's possible to have access to the reward values inside the state


class Policy
{
public:

    Policy(policyMap PolicyMap);

    Policy();

    Actions *getPolicyValuesForStateX(numberOfState NumState);//get the row of rewards by going to the next state

    policyMap getPolicyMap();

    std::string toString(int iterations, double alpha, double gamma);

    std::string toString_2(int iterations, double alpha, double gamma);

private:

    policyMap myPolicyMap;

};




#endif // POLIC_H
