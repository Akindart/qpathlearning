#include "problem.h"

Problem::Problem()
{

    this->thisGraphProblem = new GraphProblem();
    this->myPolicy = new Policy();
    this->QStore = new Policy();

}

Problem::Problem(int numStates)
{

    this->thisGraphProblem = new GraphProblem(numStates);
    this->myPolicy = new Policy();
    this->QStore = new Policy();

}

GraphProblem *Problem::getThisGraphProblem()
{

    return this->thisGraphProblem;

}

Policy *Problem::getMyPolicy()
{

    return this->myPolicy;

}

Policy *Problem::getQStore()
{

    return this->QStore;

}

State *Problem::getRandomState()
{

    return this->getThisGraphProblem()->getRandomState();

}



