#ifndef CHARACTER_H
#define CHARACTER_H

#include <QObject>
#include <QTimer>

class Character : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int x_pos READ x_pos WRITE setX_pos NOTIFY x_posChanged)
    Q_PROPERTY(int y_pos READ y_pos WRITE setY_pos NOTIFY y_posChanged)
    Q_PROPERTY(bool isAnim READ isAnim WRITE setIsAnim NOTIFY isAnimChanged)
    Q_PROPERTY(int state READ state)

    int char_x_pos;
    int char_y_pos;
    bool char_isAnim;

    QTimer timer;

public:
    explicit Character(QObject *parent = 0);

    explicit Character(int x, int y, QObject *parent = 0);

    int x_pos();
    int y_pos();
    bool isAnim();

    void setX_pos(int x);
    void setY_pos(int y);
    void setIsAnim(bool newValue);


    Q_INVOKABLE void moveChar();

    Q_INVOKABLE int state();

signals:

    void x_posChanged();
    void y_posChanged();
    void isAnimChanged();

public slots:

};

#endif // CHARACTER_H
