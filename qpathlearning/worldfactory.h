#ifndef WORLDFACTORY_H
#define WORLDFACTORY_H

#include <QObject>
#include <QString>
#include <QImage>
#include "world.h"

class WorldFactory : public QObject
{
    Q_OBJECT
public:
    explicit WorldFactory(QObject *parent = 0);

    World *createWorld(QString imagePath);
    World *createWorldWithPoints(QString imagePath);
    void createWorldWithPoints(QString imagePath, World *world);


    signals:

public slots:

};

#endif // WORLDFACTORY_H
