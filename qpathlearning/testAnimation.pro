TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

SOURCES += main.cpp \
    character.cpp \
    world.cpp \
    worldfactory.cpp \
    /home/spades/qt_ws/ql_project/QLearning/state.cpp \
    /home/spades/qt_ws/ql_project/QLearning/graphproblem.cpp \
    /home/spades/qt_ws/ql_project/QLearning/policy.cpp \
    /home/spades/qt_ws/ql_project/QLearning/problem.cpp \
    /home/spades/qt_ws/ql_project/QLearning/Utils.cpp \
    /home/spades/qt_ws/ql_project/QLearning/qlearningsolver.cpp \
    /home/spades/qt_ws/ql_project/QLearning/actions.cpp \
    /home/spades/qt_ws/ql_project/QLearning/filereader.cpp \
    qlrunner.cpp \
    animation.cpp



RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    character.h \
    world.h \
    worldfactory.h \
    /home/spades/qt_ws/ql_project/QLearning/state.h \
    /home/spades/qt_ws/ql_project/QLearning/graphproblem.h \
    /home/spades/qt_ws/ql_project/QLearning/policy.h \
    /home/spades/qt_ws/ql_project/QLearning/problem.h \
    /home/spades/qt_ws/ql_project/QLearning/Utils.h \
    /home/spades/qt_ws/ql_project/QLearning/qlearningsolver.h \
    /home/spades/qt_ws/ql_project/QLearning/actions.h \
    /home/spades/qt_ws/ql_project/QLearning/filereader.h \
    qlrunner.h \
    animation.h
