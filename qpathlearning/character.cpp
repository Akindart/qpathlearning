#include "character.h"


Character::Character(QObject *parent) :
    QObject(parent)
{
    this->char_x_pos = 0;
    this->char_y_pos = 0;
    this->char_isAnim = false;


}

Character::Character(int x, int y, QObject *parent):
    QObject(parent)
{

    this->char_x_pos = x*16;
    this->char_y_pos = y*16;
    this->char_isAnim = false;

}

int Character::x_pos()
{
    return this->char_x_pos;
}

int Character::y_pos()
{
    return this->char_y_pos;
}

bool Character::isAnim()
{
    return this->char_isAnim;
}

void Character::setX_pos(int x)
{
    if(this->char_x_pos % 16 == 0){
        if(x < 40*16 && x >= 0){
            this->char_x_pos = x;
            emit this->x_posChanged();
        }
    }
}

void Character::setY_pos(int y)
{

    if(this->char_y_pos % 16 == 0){
        if(y < 29*16 && y >= 0){
            this->char_y_pos = y;
            emit this->y_posChanged();
        }
    }


}

void Character::setIsAnim(bool newValue)
{
    this->char_isAnim = newValue;

    emit this->isAnimChanged();

}

void Character::moveChar()
{



}

int Character::state()
{

    return ((this->char_x_pos/16)*28) + (this->char_y_pos/16);

}
