#ifndef ANIMATION_H
#define ANIMATION_H

#include <QObject>
#include "character.h"
#include "world.h"


class animation : public QObject
{
    Q_OBJECT
public:
    explicit animation(QObject *parent = 0);

    Q_INVOKABLE void animate(Character *character, World *world);

signals:

public slots:

};

#endif // ANIMATION_H
