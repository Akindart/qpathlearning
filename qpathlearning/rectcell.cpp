#include "rectcell.h"

rectCell::rectCell(QColor color, QObject *parent) :
    QObject(parent)
{

    this->color_original = color;
    this->temp_color = QColor("red");

}


QColor rectCell::colorValue()
{

    return this->color_original;

}

QColor rectCell::tempColor()
{
    return this->temp_color;
}

void rectCell::setColorValue(QColor color)
{
    this->color_original = color;
}
