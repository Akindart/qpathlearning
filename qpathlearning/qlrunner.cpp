#include "qlrunner.h"
#include <fstream>


QLRunner::QLRunner(QObject *parent) :
    QObject(parent)
{
}

QString QLRunner::getProblemSolved(QString qlfPath, QString iterations, QString alpha, QString gamma, QString rho, QString nu, QString goal, World *world, QString additionalReward, QString initialState)
{

    int num_iterations = iterations.toInt();
    double alpha_value = alpha.toDouble();
    double gamma_value = gamma.toDouble();
    double rho_value = rho.toDouble();
    double nu_value = nu.toDouble();
    int goal_value = goal.toDouble();
    int additionalReward_value = additionalReward.toInt();
    int init_state_value = initialState.toInt();

    std::cout << alpha_value << std::endl;

    world->writeToFileWorldStates(qlfPath, goal_value, additionalReward_value);

    FileReader fr;

    world->setProblem(fr.ReadFile(qlfPath.toStdString()));

    QLearningSolver myQLS;

    myQLS.QLearningAlgorithm(world->getProblem(), num_iterations, alpha_value, gamma_value, rho_value, nu_value, goal_value, init_state_value);

    ofstream outPutFile;

    outPutFile.open("SimulationFile.txt",  std::ofstream::out);

    outPutFile << world->getProblem()->getQStore()->toString_2(num_iterations, alpha_value, gamma_value);

//    for(int i=0; i< 1120; i++){

//        std::cout << i << "-->" << world->getProblem()->getQStore()->getPolicyValuesForStateX(i)->findMaxReward()->first->getNumState() << " : " << world->getProblem()->getQStore()->getPolicyValuesForStateX(i)->findMaxReward()->second << std::endl;

//    }

    return NULL;
}
