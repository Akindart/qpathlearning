TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

SOURCES += main.cpp \
    character.cpp \
    world.cpp \
    worldfactory.cpp \
    QLA/state.cpp \
    QLA/graphproblem.cpp \
    QLA/policy.cpp \
    QLA//problem.cpp \
    QLA/Utils.cpp \
    QLA/qlearningsolver.cpp \
    QLA/actions.cpp \
    QLA/filereader.cpp \
    qlrunner.cpp \
    animation.cpp \
    rectcell.cpp


TARGET = qpathlearning

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    character.h \
    world.h \
    worldfactory.h \
    QLA/state.h \
    QLA/graphproblem.h \
    QLA/policy.h \
    QLA/problem.h \
    QLA/Utils.h \
    QLA/qlearningsolver.h \
    QLA/actions.h \
    QLA/filereader.h \
    qlrunner.h \
    animation.h \
    rectcell.h
