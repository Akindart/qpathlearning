import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.1

ApplicationWindow {
    visible: true
    width: 960
    height: 448
    color: "black"

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
            //Qt.quit()
        }
        onRejected: {
            console.log("Canceled")
            //Qt.quit()
        }
        //Component.onCompleted: visible = true
    }


    Row{
        id: myRows
        Repeater{
            id: rowRepeater
            model:40
            Column{
                id: myColumn
                Repeater{
                    id: columnRepeater
                    model: 28
                    Rectangle{
                        width: 16
                        height: 16
                        border.width: 1
                        border.color:"black"
                        color: world.getRectCellColor();


                    }
                }
            }
        }
    }

    Item{

        Item{
            Text{
                x: 660
                y: 20
                text:  "alpha_value :"
                color: "grey"
            }
            TextField {
                id: alphaField
                x: 755
                y: 20
                width: 30
                height: 20
                text: "0.3"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 810
                y: 20
                text:  "gamma_value :"
                color: "grey"
            }
            TextField {
                id: gammaField
                x: 918
                y: 20
                width: 30
                height: 20
                text: "0.7"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 40
                text:  "rho_value :"
                color: "grey"
            }
            TextField {
                id: rhoField
                x: 755
                y: 40
                width: 30
                height: 20
                text: "0.0"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 810
                y: 40
                text:  "nu_value :"
                color: "grey"
            }
            TextField {
                id: nuField
                x: 918
                y: 40
                width: 30
                height: 20
                text: "0.0"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 60
                text:  "number_iteratios :"
                color: "grey"
            }
            TextField {
                id: iterationsField
                x: 794
                y: 60
                width: 70
                height: 20
                text: "100000"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 870
                y: 60
                text:  "goal :"
                color: "grey"
            }
            TextField {
                id: goalField
                x: 918
                y: 60
                width: 30
                height: 20
                text: "27"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 100
                text:  "grass effort(light green rects) :"
                color: "grey"
            }
            TextField {
                id: grassField;
                x: 900
                y: 100
                width: 45
                height: 20
                text: "0.05"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 120
                text:  "forest effort(dark green rects) :"
                color: "grey"
            }
            TextField {
                id: forestField;
                x: 900
                y: 120
                width: 45
                height: 20
                text: "0.1"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 140
                text:  "water effort(blue rects) :"
                color: "grey"
            }
            TextField {
                id: waterField;
                x: 900
                y: 140
                width: 45
                height: 20
                text: "0.14"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 160
                text:  "swamp effort(brown rects) :"
                color: "grey"
            }
            TextField {
                id: swampField;
                x: 900
                y: 160
                width: 45
                height: 20
                text: "0.17"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 180
                text:  "earth effort(orange rects) :"
                color: "grey"
            }
            TextField {
                id: earthtField;
                x: 900
                y: 180
                width: 45
                height: 20
                text: "0.07"
                textColor: "black"
            }
        }
        Item{
            Text{
                x: 660
                y: 200
                text:  "another effort (any other color):"
                color: "grey"
            }
            TextField {
                id: anotherField;
                x: 900
                y: 200
                width: 45
                height: 20
                text: "0.07"
                textColor: "black"
            }
        }


        Item{

            Text{

                x: 660
                y: 255
                text: "Transition function between rects : \n\n[(1-effort)^2]/(dist_weight * dist_to_goal) \n\n \t\tor \n\n [(1-effort)^2]/(dist_weight * 0.00001) \n\n when the next state is the goal!"
                color: "grey"
            }


            Text{
                x: 660
                y: 220
                text:  "distance weight :"
                color: "grey"
            }
            TextField {
                id: distancField;
                x: 900
                y: 220
                width: 45
                height: 20
                text: "0.5"
                textColor: "black"
            }
        }



        Button{

            x: 660
            y: 410
            id: ql_button
            action:Action{

                onTriggered: {

                    world.setCost_grass(grassField.text);
                    world.setCost_forest(forestField.text);
                    world.setCost_water(waterField.text);
                    world.setCost_swamp(swampField.text);
                    world.setCost_earth(earthtField.text);
                    world.setCost_another(anotherField.text);

                    world.setDistance_weight(distancField.text);

                    world.setGoal(goalField.text)

                    myRows.children[character.x_pos/16].children[character.y_pos/16].color = "purple"

                    console.log(goalField.text)

                    qlrunner.getProblemSolved("testQt.qlf", iterationsField.text, alphaField.text, gammaField.text, rhoField.text, nuField.text, goalField.text, world, "100000", character.state);

                    charItem.forceActiveFocus()

                }

                tooltip: "Makes QLearning Algorithm runs creating a whole new states' links with the values of the other fields";

            }
            text: "Run QLearning Algorithm"

        }
        Button{

            x: 870
            y: 410
            id: foward_button
            width: 50
            action: Action{

                onTriggered: {

                    charItem.forceActiveFocus()

                    animate.animate(character, world);

                    if(character.state == world.goal) myRows.children[character.x_pos/16].children[character.y_pos/16].color = "silver"

                    else if(myRows.children[character.x_pos/16].children[character.y_pos/16].color != "purple") myRows.children[character.x_pos/16].children[character.y_pos/16].color = "red"



                }
                tooltip: "Execute the movement of the yellow rect agent, it will crash if you do not run the button on the left first! =D";



            }

            text: "RUN!"

        }

    }

    Item{

        id: charItem
        focus: true

        Rectangle{
            id: myChar
            x: character.x_pos
            y: character.y_pos
            width: 16
            height: 16
            border.width: 1
            border.color: "black"
            gradient: Gradient { // This sets a vertical gradient fill
                GradientStop { position: 0.0; color: "snow" }
                GradientStop { position: 1.0; color: "yellow" }
            }

            // This is the behavior, and it applies a NumberAnimation to any attempt to set the x property
            Behavior on x {

                NumberAnimation {
                    //This specifies how long the animation takes
                    duration: 0
                    //This selects an easing curve to interpolate with, the default is Easing.Linear
                    easing.type: Easing.InOutQuad

                }

            }

            Behavior on y {

                NumberAnimation {

                    //This specifies how long the animation takes
                    duration: 0

                    //This selects an easing curve to interpolate with, the default is Easing.Linear
                    easing.type: Easing.InOutQuad

                }

            }

            MouseArea{

                onClicked: charItem.forceActiveFocus()

            }

        }



        Keys.onPressed: {

            if(event.key == Qt.Key_Q){

                world.setCost_grass(grassField.text);
                world.setCost_forest(forestField.text);
                world.setCost_water(waterField.text);
                world.setCost_swamp(swampField.text);
                world.setCost_earth(earthtField.text);
                world.setCost_another(anotherField.text);

                world.setDistance_weight(distancField.text);

                world.setGoal(goalField.text)

                myRows.children[character.x_pos/16].children[character.y_pos/16].color = "purple"

                console.log(goalField.text)

                qlrunner.getProblemSolved("testQt.qlf", iterationsField.text, alphaField.text, gammaField.text, rhoField.text, nuField.text, goalField.text, world, "100000", character.state);

            }


            if(event.key == Qt.Key_F){


                animate.animate(character, world);

                if(character.state == world.goal) myRows.children[character.x_pos/16].children[character.y_pos/16].color = "silver"


                else if(myRows.children[character.x_pos/16].children[character.y_pos/16].color != "purple") myRows.children[character.x_pos/16].children[character.y_pos/16].color = "red"

            }

            if(event.key == Qt.Key_Right)character.x_pos = myChar.x + 16;
            if(event.key == Qt.Key_Left) character.x_pos = myChar.x - 16;
            if(event.key == Qt.Key_Down) character.y_pos = myChar.y + 16;
            if(event.key == Qt.Key_Up) character.y_pos = myChar.y - 16;

        }





    }



    Item{

        id: divider

        Rectangle {
            x: 647
            y: 0
            height: 225
            width: 3
            gradient: Gradient { // This sets a vertical gradient fill
                GradientStop { position: 0.0; color: "black" }
                GradientStop { position: 1.0; color: "grey" }
            }
        }
        Rectangle {
            x: 647
            y: 225
            height: 225
            width: 3
            gradient: Gradient { // This sets a vertical gradient fill
                GradientStop { position: 0.0; color: "grey" }
                GradientStop { position: 1.0; color: "black" }
            }
        }
    }





}
