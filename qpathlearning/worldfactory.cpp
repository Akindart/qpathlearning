#include "worldfactory.h"
#include <iostream>

WorldFactory::WorldFactory(QObject *parent) :
    QObject(parent)
{
}

World *WorldFactory::createWorld(QString imagePath)
{
    int pixelJump_column;
    int pixelJump_row;

    QString extension = QString("." + imagePath.split(".")[1]);
    QImage qimage(imagePath, extension.toStdString().c_str());

    pixelJump_column = qimage.width()/40;
    pixelJump_row = qimage.height()/28;

    World *world = new World();

    for(int col=0; col<40*pixelJump_column; col += pixelJump_column)
        for(int row=0; row<28*pixelJump_row; row += pixelJump_row){
            world->addToWorldColorsString(QColor(qimage.pixel(col,row)).name());
            std::cout << world->getWorldColorsS().back().toStdString() << std::endl;
        }
    return world;

}

World *WorldFactory::createWorldWithPoints(QString imagePath)
{
    int pixelJump_column;
    int pixelJump_row;

    QString extension = QString("." + imagePath.split(".")[1]);
    QImage qimage(imagePath, extension.toStdString().c_str());

    pixelJump_column = qimage.width()/40;
    pixelJump_row = qimage.height()/28;

    World *world = new World();

    for(int col=0; col<40*pixelJump_column; col += pixelJump_column)
        for(int row=0; row<28*pixelJump_row; row += pixelJump_row){
            world->addToWorldColors(QColor(qimage.pixel(col,row)), col, row);
            //std::cout << world->getWorldColorsS().back().toStdString() << std::endl;
        }
    return world;

}

void WorldFactory::createWorldWithPoints(QString imagePath, World *world)
{
    int pixelJump_column;
    int pixelJump_row;

    QString extension = QString("." + imagePath.split(".")[1]);
    QImage qimage(imagePath, extension.toStdString().c_str());

    pixelJump_column = qimage.width()/40;
    pixelJump_row = qimage.height()/28;

    free(world);

    world = new World();

    for(int col=0; col<40*pixelJump_column; col += pixelJump_column)
        for(int row=0; row<28*pixelJump_row; row += pixelJump_row){
            world->addToWorldColors(QColor(qimage.pixel(col,row)), col, row);
            //std::cout << world->getWorldColorsS().back().toStdString() << std::endl;
        }

    return ;

}
