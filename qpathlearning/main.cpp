#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQmlComponent>
#include <QQmlApplicationEngine>
#include "character.h"
#include "worldfactory.h"
#include "world.h"
#include "qlrunner.h"
#include "animation.h"



int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    int char_xStart = 39;
    int char_yStart = 0;


    Character m_char(char_xStart, char_yStart);
    WorldFactory wFactory;
    QLRunner qlRunner;
    animation anmation;

    std::cout << m_char.state() << std::endl;

    World *world = wFactory.createWorldWithPoints(argv[1]);

    engine.rootContext()->setContextProperty("character", &m_char);
    engine.rootContext()->setContextProperty("world", world);
    engine.rootContext()->setContextProperty("animate", &anmation);
    engine.rootContext()->setContextProperty("qlrunner", &qlRunner);

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));


    return app.exec();
}
